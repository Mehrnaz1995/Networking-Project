#!/usr/bin/python           # This is client.py file

import socket               # Import socket module

host = socket.gethostname() # Get local machine name
port = 12358                # Reserve a port for your service.

while True:
    s = socket.socket()
    s.connect(('localhost', port))
    message_to_be_sent = input()
    s.send(message_to_be_sent.encode('utf-8'))
    print(s.recv(1024).decode('utf-8'))
    s.close()
