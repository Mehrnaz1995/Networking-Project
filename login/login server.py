import socket               # Import socket module
import threading
import json


def broadcast(message):
    for client in clients:
        client.send(message)


def send_to_one_client(data, connection):
    connection.send(data)


class BroadcastHandler(threading.Thread):
    def __init__(self, name,connection):
        threading.Thread.__init__(self)
        self.name = name
        self.port = 12363  # Reserve a port for your service.
        self.connection = connection

    def run(self):
        while True:
            response = {}
            print("server:in"+str(self.name)+" thread while")
            json_data = self.connection.recv(1024).decode('utf-8')
            data_dictionary = json.loads(json_data)
            # TODO what are the actions
            if data_dictionary["action_type"] == "register":
                if data_dictionary["username"] in username_database:
                    response["type"] = "register"
                    response["success"] = False
                    response["error"] = "duplicate username"
                elif len(data_dictionary["password"]) < 3:
                    response["type"] = "register"
                    response["success"] = False
                    response["error"] = "short password"
                else:
                    response["type"] = "register"
                    response["success"] = True
                    username_database[data_dictionary["username"]] = data_dictionary["password"]
            if data_dictionary["action_type"] == "login":
                if not(data_dictionary["username"] in username_database):
                    response["type"] = "login"
                    response["success"] = False
                    response["error"] = "wrong username"
                elif username_database[data_dictionary["username"]] != data_dictionary["password"]:
                    response["type"] = "login"
                    response["success"] = False
                    response["error"] = "wrong password"
                else:
                    response["type"] = "login"
                    response["success"] = True
            if data_dictionary["action_type"] == "public_message":
                response["type"] = "public_message"
                response["from"] = self.name
                response["message"] = data_dictionary["message"]



            json_decoded_response = json.dumps(response).encode('utf-8')
            send_to_one_client(json_decoded_response, self.connection)

            # broadcast(data)


def on_new_client(client_socket, addr):
    while True:
        msg = client_socket.recv(1024)
        client_socket.send(msg)
        client_socket.close()


#variables
username_database = {}


s = socket.socket()
host = socket.gethostname()
port = 12364
s.bind(('localhost', port))
s.listen(5)
clients = []
connection_number = 0
while True:
        connection_number += 1
        c, address = s.accept()  # Establish connection with client.
        clients.append(c)
        new_client = BroadcastHandler(connection_number, c)
        new_client.start()
        print('Got connection from', address)
